import numpy as np
import matplotlib.pyplot as plt
from numpy.random import randn


def brown1(Nt,M,dt=1):
    """Run M Brownian motion simulations each consisting of Nt time steps
    with time step = dt
    Returns: X: the M trajectories; Xm: the mean across these M samples; Xv:
    the variance across these M samples
    """
    #Initialize variable
    X = np.zeros((M,Nt+1))

    #1D Brownian motion: X_j+1 = X_j + sqrt(dt)*N(0,1)
    for i in range(M):
        for j in range(Nt):
            X[i,j+1] = X[i,j] + np.sqrt(dt)*randn(1)

    Xm = np.mean(X,axis=0)
    Xv = np.var(X,axis=0)
    return X,Xm,Xv


def analyze(display=False):
    """Lab2 task 2: Compute variance in M Brownian motion simulations with varying M
    Compute and return error along with M values and variances.
    Plot error if input variable display=True
    To run this function, first import brown in python terminal, then: brown.analyze(True)
    """

    Mvalues = [100,1000,10000,100000] #Compute error for these values of M
    Nt=100
    Xvarray = np.zeros(len(Mvalues)) #initialize array to store variances at t=Nt+1
    errorv = np.zeros(len(Mvalues))
    #Compute variances for each M
    for i,M in enumerate(Mvalues):
        _,_,Xv = brown1(Nt,M)
        print(i,M,Xv[-1])
        Xvarray[i] = Xv[-1]
        t = np.arange(Nt+1)

    errorv = np.abs(Xvarray-Nt)
    return Mvalues, Xvarray, errorv