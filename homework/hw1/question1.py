#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 16 13:13:22 2018

@author: apple
"""
import numpy as np

def simulate1(N,Nt,b,e):
    """Simulate C vs. M competition on N x N grid over
    Nt generations. b and e are model parameters
    to be used in fitness calculations
    Output: S: Status of each gridpoint at tend of somulation, 0=M, 1=C
    fc: fraction of villages which are C at all Nt+1 times
    Do not modify input or return statement without instructor's permission.
    """
    #Set initial condition
    S  = np.ones((N,N),dtype=int)#Status of each gridpoint: 0=M, 1=C
    j = int((N-1)/2)
    S[j-1:j+2,j-1:j+2] = 0
    fc = np.zeros(Nt+1) #Fraction of points which are C
    fc[0] = S.sum()/(N*N)
    
    neighbours=8*np.ones((N,N),dtype=int)
    
    """first of all, we need to calculate the number of neighbors for each entry"""
    neighbours[0,0]=3
    neighbours[0,N-1]=3
    neighbours[N-1,0]=3
    neighbours[N-1,N-1]=3
    neighbours[0,range(1,N-1,1)]=5
    neighbours[N-1,range(1,N-1,1)]=5
    neighbours[range(1,N-1,1),0]=5
    neighbours[range(1,N-1,1),N-1]=5
    
    points=np.zeros((N,N),dtype=float)
    pmat=np.zeros((N,N),dtype=float)
    pmatexpand=np.zeros((N+2,N+2),dtype=float)
    pmatexpand[1:N+1,1:N+1]=pmat
    pmatCexpand=np.zeros((N+2,N+2),dtype=float)
    ftcoreexpand=np.zeros((N+2,N+2),dtype=float)

    for k in range(Nt):
        Sexpand=np.zeros((N+2,N+2),dtype=int)
        Sexpand[1:N+1,1:N+1]=S
        
        #calculate the number of neighbours that are M villiages for each village
        Cnbs=Sexpand[0:N,0:N]+Sexpand[1:N+1,0:N]+Sexpand[2:N+2,0:N]+\
        Sexpand[0:N,1:N+1]+Sexpand[2:N+2,1:N+1]+Sexpand[0:N,2:N+2]+\
        Sexpand[1:N+1,2:N+2]+Sexpand[2:N+2,2:N+2]
        Mnbs=neighbours-Cnbs
        #calculate points for C village
        Cvlg=np.where(S==1)
        points[Cvlg]=Cnbs[Cvlg]
        #calculate points for M village
        Mvlg=np.where(S==0)
        points[Mvlg]=b*Cnbs[Mvlg]+e*Mnbs[Mvlg]
        fitscore=points/neighbours
        ftcoreexpand[1:N+1,1:N+1]=fitscore
        ftscoretotal=ftcoreexpand[0:N,0:N]+ftcoreexpand[1:N+1,0:N]+ftcoreexpand[2:N+2,0:N]+\
        ftcoreexpand[0:N,1:N+1]+ftcoreexpand[2:N+2,1:N+1]+ftcoreexpand[0:N,2:N+2]+\
        ftcoreexpand[1:N+1,2:N+2]+ftcoreexpand[2:N+2,2:N+2]
        
        #calculate the probability of being a C village 
        pointsC=fitscore*S
        pmatCexpand[1:N+1,1:N+1]=pointsC
        #calculate the totl number of fitness scores of each C points
        ftscoreC=pmatCexpand[0:N,0:N]+pmatCexpand[1:N+1,0:N]+pmatCexpand[2:N+2,0:N]+\
        pmatCexpand[0:N,1:N+1]+pmatCexpand[2:N+2,1:N+1]+pmatCexpand[0:N,2:N+2]+\
        pmatCexpand[1:N+1,2:N+2]+pmatCexpand[2:N+2,2:N+2]
        pmatC=ftscoreC/ftscoretotal
        
        """#calculate the probability of being an M village for M villages
        pointsM=fitscore*Sinv
        pmatMexpand[1:N+1,1:N+1]=pointsM
        #calculate the totl number of fitness scores of each C points
        ftscoreM=pmatMexpand[0:N,0:N]+pmatMexpand[1:N+1,0:N]+pmatMexpand[2:N+2,0:N]+\
        pmatMexpand[0:N,1:N+1]+pmatMexpand[2:N+2,1:N+1]+pmatMexpand[0:N,2:N+2]+\
        pmatMexpand[1:N+1,2:N+2]+pmatMexpand[2:N+2,2:N+2]
        pmatM=ftscoreM/ftscoretotal"""
        #calculate the the identities of matrices after one step:
        S=np.zeros((N,N),dtype=int)
        for i in range(N):
            for j in range(N):
                S[i,j]=np.random.choice([1,0],p=[pmatC[i,j],1-pmatC[i,j]])
        
        fc[k+1] = S.sum()/(N*N)
        
    return S,fc